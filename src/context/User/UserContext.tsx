import { createContext } from 'react';
import { IUserState } from '../../models/user-state.interface';

//The Typescript Partial allows the context to initialize with an empty object. The data will later initialize fully through the context Provider.

const UserContext = createContext<Partial<any>>({});

export default UserContext;