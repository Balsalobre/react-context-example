import React from 'react';
import { IAction } from '../../models/action.interface';
import { IUserState } from '../../models/user-state.interface';
import { IUser } from '../../models/user.iterface';
import * as userActions from '../UserActions';

const mock: IUser = {id: 0, email: 'j@gmail.com', first_name: 'string', last_name: 'string', avatar: 'avatar' }

export const UserReducer: React.Reducer<IUserState, IAction> = (state, action) => {
    switch (action.type) {
      case userActions.GET_USERS:
        return {
          ...state, users: action.payload
        };
      case userActions.GET_PROFILE:
        return {
          ...state, selectedUser: action.payload  
        };
      default:
        return state;
    }
  }
