import React, { useReducer, Reducer } from 'react';
import { UserReducer } from './UserReducer';
import UserContext from './UserContext';
import { IUserState } from '../../models/user-state.interface';
import axios from 'axios';
import { IAction } from '../../models/action.interface';

const UserState = (props: any) => {
    const initialState: IUserState = {
        users:  [],
        selectedUser: null
    };

    const [state, dispatch] = useReducer<Reducer<IUserState, IAction>>(UserReducer, initialState);    
    const options = {
        headers: {"Access-Control-Allow-Origin": "*"}
    };
    const getUsers = async () => {
       const res = await axios.get('https://reqres.in/api/users', options);
       dispatch({
            type: 'GET_USERS',
            payload: res.data.data
       });
    }

    const getProfile = async (id: number) => {
        const res = await axios.get(`https://reqres.in/api/users/${id}`, options);
        dispatch({
            type: 'GET_PROFILE',
            payload: res.data.data
        });
    }

    return (
        <UserContext.Provider value= {{
            users: state.users,
            selectedUser: state.selectedUser,
            getUsers,
            getProfile
        }}>
            { props.children }
        </UserContext.Provider>
    )
}

export default UserState;