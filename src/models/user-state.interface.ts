import { IUser } from "./user.iterface";

export interface IUserState {
    users: IUser[];
    selectedUser: IUser | null;
}